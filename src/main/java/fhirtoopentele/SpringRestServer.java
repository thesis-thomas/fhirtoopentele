package fhirtoopentele;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.BundleEntry;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Questionnaire;
import ca.uhn.fhir.model.dstu2.resource.QuestionnaireResponse;
import ca.uhn.fhir.model.dstu2.resource.QuestionnaireResponse.GroupQuestion;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.DecimalDt;
import ca.uhn.fhir.model.primitive.InstantDt;
import ca.uhn.fhir.model.primitive.IntegerDt;
import ca.uhn.fhir.model.primitive.StringDt;   


@RestController  
@RequestMapping("/")  
public class SpringRestServer {  
	private FhirContext ctx = FhirContext.forDstu2();
	HttpClient httpClient = HttpClientBuilder.create().build();
	private String openteleServer = "http://192.168.56.101:8080/opentele-citizen-server/rest/questionnaire/listing"; 
	private HashMap<String, String> getSeverity = new HashMap<String, String>();
	private HashMap<String, String> getOpenteleCode = new HashMap<String, String>();
	private String typeString = "String";
	private String typeBoolean = "Boolean";
	private String typeInteger = "Integer";
	private String typeFloat = "Float";

	public SpringRestServer() {
		getSeverity.put("N", "GREEN");
		getSeverity.put("A", "YELLOW");
		getSeverity.put("AA", "RED");

		getOpenteleCode.put("NPU02187", "286.BS#BLOODSUGARMEASUREMENTS");
		getOpenteleCode.put("DNK05472", ".BP#SYSTOLIC");
		getOpenteleCode.put("DNK05473", ".BP#DIASTOLIC");
		getOpenteleCode.put("NPU21692", ".BP#PULSE");
		getOpenteleCode.put("NPU03804", "307.WEIGHT");
	}

	@RequestMapping(value = "/hello/{name}", method = RequestMethod.GET)  
	public String hello(@PathVariable String name) {    
		return "Hello " + name;  
	}  

	@RequestMapping(value = "/postFhir", method = RequestMethod.POST)
	public ResponseEntity<String> postFhir(@RequestBody String fhir, @RequestHeader("Authorization") String encoding) {
		System.out.println("Fhir is: " + fhir);
		System.out.println("Authorization is: " + encoding);

		ca.uhn.fhir.model.api.Bundle bundle = ctx.newXmlParser().parseBundle(fhir);
		System.out.println("Parsed: " +  bundle.toString());

		List<BundleEntry> list = bundle.getEntries();
		Patient p = bundle.getResources(Patient.class).get(0);
		Observation o = bundle.getResources(Observation.class).get(0);

		long QuestionnaireId = Long.valueOf(o.getCode().getCoding().get(0).getCode()).longValue();
		System.out.println("QuestionnaireId: " + QuestionnaireId);

		//Get values from observation
		String name = o.getCode().getText();
		String version = o.getCode().getCoding().get(0).getVersion();
		InstantDt dateTemp = o.getIssuedElement();
		String date = dateTemp.getValueAsString();

		HashMap<String, Object> mapJsonObect = new HashMap<String, Object>();
		HashMap<Integer, Object> mapJsonArray = new HashMap<Integer, Object>();
		//Observation
		/*
		String name = o.getCode().getText();
		long QuestionnaireId = Long.valueOf(o.getCode().getCoding().get(0).getCode()).longValue();
		String version = o.getCode().getCoding().get(0).getVersion();
		String outputName = o.getCode().getCoding().get(0).getSystem();
		String outputType = o.getCode().getCoding().get(0).getDisplay();
		QuantityDt qd = (QuantityDt) o.getValue();
		String measurementsResult = qd.getValue().toString();
		BooleanDt bd = (BooleanDt) o.getUndeclaredExtensionsByUrl("isBeforeMeal").get(0).getValue();
		boolean measurementsIsBeforeMeal = bd.getValue(); //if false also isAfterMeal
		String measurementsTimeOfMeasurement = o.getIssued().toString(); //Might need to be adjusted to right format
		String transferTime = o.getUndeclaredExtensionsByUrl("transferTime").get(0).getValue().toString();
		String severityName = o.getInterpretation().getCoding().get(0).getDisplay();
		String severityValue = o.getInterpretation().getCoding().get(0).getCode();
		String severityType = o.getInterpretation().getCoding().get(0).getSystem();
		String date = "No date here yet!";
		 */

		try {

			switch (Long.toString(QuestionnaireId)) {
			case "40": {

				boolean measurementsIsBeforeMeal = true;

				//Get before/after meal value
				try {
					BooleanDt bd = (BooleanDt) o.getUndeclaredExtensionsByUrl("isBeforeMeal").get(0).getValue();
				} catch (IndexOutOfBoundsException e) {
					measurementsIsBeforeMeal = false;
				}

				//Get result
				QuantityDt qd = (QuantityDt) o.getValue();
				long measurementsResult = qd.getValue().longValue();

				//Get output values
				String tempOutputName = o.getCode().getCoding().get(1).getCode();
				String outputName = getOpenteleCode.get(tempOutputName);
				String outputType = o.getCode().getCoding().get(0).getDisplay();

				//Get transfer time
				String transferTime = o.getUndeclaredExtensionsByUrl("transferTime").get(0).getValue().toString();

				//Get time of measurement
				//Might need to be adjusted to right format
				DateTimeDt tempMeasurementsTimeOfMeasurement = (DateTimeDt) o.getEffective();
				String measurementsTimeOfMeasurement = tempMeasurementsTimeOfMeasurement.getValueAsString(); 

				//Get severity
				String severityName = o.getInterpretation().getText();
				String severityValueTemp = o.getInterpretation().getCoding().get(0).getCode();
				String severityValue = getSeverity.get(severityValueTemp);
				String severityType = typeString;

				//Put values in json questionnaire
				mapJsonObect.put("result", measurementsResult);
				if (!measurementsIsBeforeMeal) {
					mapJsonObect.put("isAfterMeal", true);
				} else {
					mapJsonObect.put("isBeforeMeal", true);
				}
				mapJsonObect.put("timeOfMeasurement", measurementsTimeOfMeasurement);
				JSONObject measurement1 = createJsonObjectUsingHashMap(mapJsonObect);

				//Create measurements array
				mapJsonArray.put(0, measurement1);
				JSONArray measurements1 = createJsonArrayUsingHashMap(mapJsonArray);

				//Do the value
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("measurements", measurements1);
				mapJsonObect.put("transferTime", transferTime);
				JSONObject value1 = createJsonObjectUsingHashMap(mapJsonObect);

				//Do the severity
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", severityName);
				mapJsonObect.put("value", severityValue);
				mapJsonObect.put("type", severityType);
				JSONObject output1 = createJsonObjectUsingHashMap(mapJsonObect);

				//Do the output
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", outputName);
				mapJsonObect.put("type", outputType);
				mapJsonObect.put("value", value1);
				JSONObject output0 = createJsonObjectUsingHashMap(mapJsonObect);

				//Create output array
				mapJsonArray = new HashMap<Integer, Object>();
				mapJsonArray.put(0,  output0);
				mapJsonArray.put(1,  output1);
				JSONArray combinedOutputArray = createJsonArrayUsingHashMap(mapJsonArray);

				//Combine all json
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", name);
				mapJsonObect.put("QuestionnaireId", QuestionnaireId);
				mapJsonObect.put("version", version);
				mapJsonObect.put("date", date);
				mapJsonObect.put("output", combinedOutputArray);
				JSONObject json = createJsonObjectUsingHashMap(mapJsonObect);

				System.out.println("JsonString:\n" + json.toString());

				sendDataToOpentele(json.toString(), encoding);

				System.out.println("Json:\n" + json);
				break;
			}
			case "43": {
				//Create output array				
				mapJsonArray = addComponents(mapJsonArray, o, 0);

				//Add questions
				QuestionnaireResponse qr = bundle.getResources(QuestionnaireResponse.class).get(0);
				mapJsonArray = iterateQuestions(mapJsonArray, 4, qr, 0);

				JSONArray combinedOutputArray = createJsonArrayUsingHashMap(mapJsonArray);

				//Combine all json
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", name);
				mapJsonObect.put("QuestionnaireId", QuestionnaireId);
				mapJsonObect.put("version", version);
				mapJsonObect.put("date", date);
				mapJsonObect.put("output", combinedOutputArray);
				JSONObject json = createJsonObjectUsingHashMap(mapJsonObect);

				System.out.println("JsonString:\n" + json.toString());

				sendDataToOpentele(json.toString(), encoding);

				System.out.println("Json:\n" + json);	
				break;
			}
			case "24": {
				//Create output array				
				mapJsonArray = addComponents(mapJsonArray, o, 0);

				//Add questions
				QuestionnaireResponse qr = bundle.getResources(QuestionnaireResponse.class).get(0);
				mapJsonArray = iterateQuestions(mapJsonArray, 4, qr, 0);

				JSONArray combinedOutputArray = createJsonArrayUsingHashMap(mapJsonArray);

				//Combine all json
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", name);
				mapJsonObect.put("QuestionnaireId", QuestionnaireId);
				mapJsonObect.put("version", version);
				mapJsonObect.put("date", date);
				mapJsonObect.put("output", combinedOutputArray);
				JSONObject json = createJsonObjectUsingHashMap(mapJsonObect);

				System.out.println("JsonString:\n" + json.toString());

				sendDataToOpentele(json.toString(), encoding);

				System.out.println("Json:\n" + json);
				break;
			}
			case "27": {
				
				//Add contact midwife
				QuestionnaireResponse qr = bundle.getResources(QuestionnaireResponse.class).get(0);
				
				//Create output array				
				mapJsonArray = addComponents(mapJsonArray, o, 1);
				
				GroupQuestion q = qr.getGroup().getQuestion().get(0);
				String nameContactMidwife = q.getLinkId();
				BooleanDt bd = (BooleanDt) q.getAnswer().get(0).getValue();
				boolean value = bd.getValue();
				String type = typeBoolean;

				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", nameContactMidwife);
				mapJsonObect.put("type", type);
				mapJsonObect.put("value", value);
				JSONObject output = createJsonObjectUsingHashMap(mapJsonObect);
				mapJsonArray.put(0,  output);

				//Add questions
				mapJsonArray = iterateQuestions(mapJsonArray, 5, qr, 1);
				
				System.out.println("Current: " + mapJsonArray.toString());

				JSONArray combinedOutputArray = createJsonArrayUsingHashMap(mapJsonArray);

				//Combine all json
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", name);
				mapJsonObect.put("QuestionnaireId", QuestionnaireId);
				mapJsonObect.put("version", version);
				mapJsonObect.put("date", date);
				mapJsonObect.put("output", combinedOutputArray);
				JSONObject json = createJsonObjectUsingHashMap(mapJsonObect);

				System.out.println("JsonString:\n" + json.toString());

				sendDataToOpentele(json.toString(), encoding);

				System.out.println("Json:\n" + json);
				break;
			}
			case "44": {				
				//Get values
				String tempValueName = o.getCode().getCoding().get(1).getCode();
				String valueName = getOpenteleCode.get(tempValueName);
				String valueType = typeFloat;

				//Get result
				QuantityDt qd = (QuantityDt) o.getValue();
				long result = qd.getValue().longValue();

				//Get severity
				String severityName = o.getInterpretation().getText();
				String severityValueTemp = o.getInterpretation().getCoding().get(0).getCode();
				String severityValue = getSeverity.get(severityValueTemp);
				String severityType = typeString;

				//Do the output
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", valueName);
				mapJsonObect.put("type", valueType);
				mapJsonObect.put("value", result);
				JSONObject output0 = createJsonObjectUsingHashMap(mapJsonObect);

				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", severityName);
				mapJsonObect.put("type", severityType);
				mapJsonObect.put("value", severityValue);
				JSONObject output1 = createJsonObjectUsingHashMap(mapJsonObect);


				//Create output array
				mapJsonArray = new HashMap<Integer, Object>();
				mapJsonArray.put(0,  output0);
				mapJsonArray.put(1,  output1);
				JSONArray combinedOutputArray = createJsonArrayUsingHashMap(mapJsonArray);

				//Combine all json
				mapJsonObect = new HashMap<String, Object>();
				mapJsonObect.put("name", name);
				mapJsonObect.put("QuestionnaireId", QuestionnaireId);
				mapJsonObect.put("version", version);
				mapJsonObect.put("date", date);
				mapJsonObect.put("output", combinedOutputArray);
				JSONObject json = createJsonObjectUsingHashMap(mapJsonObect);

				System.out.println("JsonString:\n" + json.toString());

				sendDataToOpentele(json.toString(), encoding);

				System.out.println("Json:\n" + json);
				break;
			}
			default: {
				//Id not supported
				break;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>(fhir, HttpStatus.OK);
	}

	private HashMap<Integer, Object> addComponents(HashMap<Integer, Object> mapJsonArray, Observation o, int offset) {
		JSONObject output = new JSONObject();
		HashMap<String, Object> mapJsonObect = new HashMap<String, Object>();
		HashMap<Integer, Object> currentMap = new HashMap<Integer, Object>();

		//Get severity
		String severityName = o.getInterpretation().getText();
		String severityValueTemp = o.getInterpretation().getCoding().get(0).getCode();
		String severityValue = getSeverity.get(severityValueTemp);
		String severityType = typeString;

		String questionnaireBeginning = severityName.substring(0, 3);

		for (int i = 0; i < 3; i++) {
			String tempComponentName = o.getComponent().get(i).getCode().getCoding().get(0).getCode();
			String componentName = questionnaireBeginning + getOpenteleCode.get(tempComponentName);
			QuantityDt tempComponentValue = (QuantityDt) o.getComponent().get(i).getValue();
			long componentValue = tempComponentValue.getValue().longValue();
			String componentType = typeInteger;
			//Do the output
			mapJsonObect = new HashMap<String, Object>();
			mapJsonObect.put("name", componentName);
			mapJsonObect.put("type", componentType);
			mapJsonObect.put("value", componentValue);
			output = createJsonObjectUsingHashMap(mapJsonObect);

			currentMap.put(i + offset, output);
		}

		//Do the severity
		mapJsonObect = new HashMap<String, Object>();
		mapJsonObect.put("name", severityName);
		mapJsonObect.put("value", severityValue);
		mapJsonObect.put("type", severityType);
		output = createJsonObjectUsingHashMap(mapJsonObect);

		currentMap.put(3 + offset, output);

		return currentMap;
	}

	private HashMap<Integer, Object> iterateQuestions(HashMap<Integer, Object> mapJsonArray, int offset, QuestionnaireResponse qr, int questionsDone) {
		System.out.println("QuestionnaireResonse: " + qr.getGroup().getQuestion().get(0).getLinkId());
		System.out.println("Size: " + qr.getGroup().getQuestion().size());
		HashMap<Integer, Object> currentMap = mapJsonArray;
		HashMap<String, Object> mapJsonObect = new HashMap<String, Object>();

		for (int i = 0 + questionsDone; i < qr.getGroup().getQuestion().size(); i ++) {
			GroupQuestion q = qr.getGroup().getQuestion().get(i);
			String classType = q.getAnswer().get(0).getValue().getClass().toString();
			String name = q.getLinkId();
			mapJsonObect.put("name", name);

			if (classType.contains("BooleanDt")) {
				BooleanDt bd = (BooleanDt) q.getAnswer().get(0).getValue();
				boolean value = bd.getValue();
				String type = typeBoolean;

				mapJsonObect.put("type", type);
				mapJsonObect.put("value", value);
			} else if (classType.contains("CodingDt")) {
				CodingDt cd = (CodingDt) q.getAnswer().get(0).getValue();
				String tempValue = cd.getCode();
				String value = getSeverity.get(tempValue);
				String type = typeString;

				mapJsonObect.put("type", type);
				mapJsonObect.put("value", value);
			} else if (classType.contains("IntegerDt")) {
				IntegerDt id = (IntegerDt) q.getAnswer().get(0).getValue();
				long value = id.getValue().longValue();
				String type = typeInteger;

				mapJsonObect.put("type", type);
				mapJsonObect.put("value", value);
			} else if (classType.contains("DecimalDt")) {
				DecimalDt dd = (DecimalDt) q.getAnswer().get(0).getValue();
				double value = dd.getValue().doubleValue();
				String type = typeFloat;

				mapJsonObect.put("type", type);
				mapJsonObect.put("value", value);
			} else {
				//Not supported
				String value = "Not Supported";
				String type = "Not Supported";

				mapJsonObect.put("type", type);
				mapJsonObect.put("value", value);
			}


			JSONObject output = createJsonObjectUsingHashMap(mapJsonObect);

			currentMap.put(i + (offset - questionsDone), output);
		}
		return currentMap;
	}

	private JSONObject createJsonObjectUsingHashMap(HashMap<String, Object> map) {
		JSONObject json = new JSONObject();
		for (java.util.Map.Entry<String, Object> entry : map.entrySet()) {
			json.put(entry.getKey(), entry.getValue());
		}
		return json;
	}

	private JSONArray createJsonArrayUsingHashMap(HashMap<Integer, Object> map) {
		JSONArray json = new JSONArray();
		int i = 0;
		for (java.util.Map.Entry<Integer, Object> entry : map.entrySet()) {
			json.add(i, entry.getValue());
			i++;
		}

		return json;
	}

	private void sendDataToOpentele(String json, String encoding) {
		HttpPost request = new HttpPost(openteleServer);
		StringEntity params;
		try {
			System.out.println("I got here");
			params = new StringEntity(json);
			request.setHeader("Authorization", encoding);
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			HttpResponse response = httpClient.execute(request);
			System.out.println("Response: " + response.toString());
			System.out.println("Response: " + response.getStatusLine());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}  



